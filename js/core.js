// Animate CSS : BEGIN

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', 'faster', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }
    
    node.addEventListener('animationend', handleAnimationEnd)
}

// Animate CSS : END 

// Lazy Load : BEGIN

var lazy_load = new LazyLoad({
    elements_selector: ".lazy"
});

lazy_load.loadAll();
// Lazy Load : END





/*************************************************************************** dragging or click ***********************************************************************************************/




//Bootbox defaults	
bootbox.setDefaults({
  backdrop: true,
  closeButton: true,
  animate: true,
  className: "uor-modal",
  centerVertical: true

});	
	
// Header Text CSS : BEGIN

function quickfitText(reset) {
    
  var width = $(window).width();
    
  if(width >= 768){
      
      var quickFitClass="quickfit";				//Base class of elements to adapt
      var quickFitGroupClass="quickfitGroup";		//Elements in a group will all have the same size
      var quickFitIndependantClass="quickfitIndependant";	//Elements with this class won't be taken for quickfitGroup (they will be independant)
      var quickFitSetClass="quickfitSet";			//Elements with size set will get this class
      var quickFitFontSizeData="quickfit-font-size";
      //Set the font-size property of your element to the MINIMUM size you want for your content

      if(reset)
      { $("."+quickFitSetClass).removeClass(quickFitSetClass); }

      //The magic happens here
      var setMaxTextSize=function(jElement) {
          //Get and set the font size into data for reuse upon resize
          var fontSize=parseInt(jElement.data(quickFitFontSizeData)) || parseInt(jElement.css("font-size"));
          jElement.data(quickFitFontSizeData,fontSize);

          //Gradually increase font size unti the element gets a big increase in height (ie line break)
          var i=0;
          var previousHeight;
          do
          {
              previousHeight=jElement.height();
              jElement.css("font-size",""+(++fontSize)+"px");
          }
          while(i++<300 && jElement.height()-previousHeight<fontSize/2)

          //Finally, go back before the increase in height and set the element as resized by adding quickFitSetClass
          fontSize-=1;
          jElement.addClass(quickFitSetClass).css("font-size",""+fontSize+"px");

          return fontSize;
      };
      $("."+quickFitClass).each(function() {
          var jThis=$(this);

          if(!jThis.hasClass(quickFitSetClass))
          {
              var jFitGroup=jThis.closest("."+quickFitGroupClass);
              if(!jThis.hasClass(quickFitIndependantClass) && jFitGroup.length>0)
              {
                  //We are in a group, set the max fit size for all
                  var minFontSize=12;
                  jFitGroup.find("."+quickFitClass+":not(."+quickFitIndependantClass+")").each(function(i,item) {
                      minFontSize=Math.min(minFontSize,setMaxTextSize($(item)));
                  }).css("font-size",""+minFontSize+"px");
              }
              else
              { setMaxTextSize(jThis); }
          }
      });
      
  }
}

$(window).on("resize orientationchange",function() {
	quickfitText(true); 
});
    
$(function(){
    $('h1').addClass('clip');
    quickfitText();	
});

// Header Text CSS : END

// Navigation : BEGIN

$(function () {
  $(document).scroll(function () {
    var $nav = $(".fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());

  });
});

function inViewport(el) {
  var top = el.offsetTop;
  var left = el.offsetLeft;
  var width = el.offsetWidth;
  var height = el.offsetHeight;

  while(el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    left += el.offsetLeft;
  }

  return (
    top < (window.pageYOffset + window.innerHeight) &&
    left < (window.pageXOffset + window.innerWidth) &&
    (top + height) > window.pageYOffset &&
    (left + width) > window.pageXOffset
);
}

var sticky_nav = false;
function remove_sticky_nav(){    
    var cta = document.getElementById("call-to-actions");
	
	if( cta ){
   
		if(inViewport(cta) && sticky_nav){

			sticky_nav = false;
			jQuery('.sticky-bottom').removeClass('slide_in');

		} else 

		if(!inViewport(cta) && !sticky_nav){
			sticky_nav = true;

			jQuery('.sticky-bottom').addClass('slide_in');

		}

	}
    
}

$(function () {
    remove_sticky_nav(); 
});

window.onscroll = function() {
    remove_sticky_nav(); 
};

	// Slide out fixed : BEIGN

	// jQuery element variables
	var $hamburgerMenuBtn,
		$slideNav,
		$slideNav_inner,
		$closeBtn,
		$main;

	// focus management variables
	var $focusableInNav,
		$firstFocusableElement,
		$lastFocusableElement;

	$(document).ready(function() {
	  $hamburgerMenuBtn = $("#hamburger-menu"),
		$slideNav = $("#slide-nav"),
		$slideNav_inner = $("#slide-nav .inner"),
		$closeBtn = $("#close-slide-nav"),
		$main = $("main"),
		$focusableInNav = $('#slide-nav button, #slide-nav [href], #slide-nav input, #slide-nav select, #slide-nav textarea, #slide-nav [tabindex]:not([tabindex="-1"])');
	  if ($focusableInNav.length) {
		$firstFocusableElement = $focusableInNav.first();
		$lastFocusableElement = $focusableInNav.last();
	  }

	  addEventListeners();
	});

	function addEventListeners() {
	  $hamburgerMenuBtn.click(openNav);
	  $closeBtn.click(closeNav);
	  $slideNav.on("keyup", closeNav);
	  $firstFocusableElement.on("keydown", moveFocusToBottom);
	  $lastFocusableElement.on("keydown", moveFocusToTop);
	}



	function openNav() {

	  const scrollY = document.documentElement.style.getPropertyValue('--scroll-y');
	  const body = document.body;
	  body.style.position = 'fixed';
	  body.style.top = `-${scrollY}`;		
		
      var add_backdrop = document.createElement("div");
      add_backdrop.setAttribute("class", "slide_nav_backdrop");
      body.appendChild(add_backdrop);		
		
		
		
	  $slideNav.addClass("visible active");

	  setTimeout(function() {
		$slideNav_inner.fadeIn();
		$firstFocusableElement.focus();
	  }, 500);	

	  $main.attr("aria-hidden", "true");
	  $hamburgerMenuBtn.attr("aria-hidden", "true");
		
	}


	function closeNav(e) {

	  if (e.type === "keyup" && e.key !== "Escape") {
		return;
	  }
		
	  const body = document.body;
	  const scrollY = body.style.top;
	  body.style.position = '';
	  body.style.top = '';
	  window.scrollTo(0, parseInt(scrollY || '0') * -1);	
		
	 $('.slide_nav_backdrop').fadeOut().remove();	
		

	  $slideNav.removeClass("active");
	  $slideNav_inner.hide();
	  $main.removeAttr("aria-hidden");
	  $hamburgerMenuBtn.removeAttr("aria-hidden");
	  setTimeout(function() {
		$hamburgerMenuBtn.focus()
	  }, 1);
	  setTimeout(function() {
		$slideNav.removeClass("visible");
	  }, 501);
	}

	function moveFocusToTop(e) {
	  if (e.key === "Tab" && !e.shiftKey) {
		e.preventDefault();
		$firstFocusableElement.focus();
	  }
	}

	function moveFocusToBottom(e) {
	  if (e.key === "Tab" && e.shiftKey) {
		e.preventDefault();
		$lastFocusableElement.focus()
	  }
	}

	window.addEventListener('scroll', () => {
	  document.documentElement.style.setProperty('--scroll-y', `${window.scrollY}px`);
	});


	// Slide out fixed : END


// Navigation : END


// Slick : BEGIN

$(document).ready(function() {
  $('.slider').slick({
	regionLabel: 'Carousel containing links and videos for further information',  
	dots: true,
    infinite: true,
    speed: 300, 
    slidesToShow: 3,
    slidesToScroll: 1,
    mobileFirst: true,
    responsive : [ { breakpoint: 1500, settings : { slidesToShow: 3 } }, { breakpoint: 990, settings : { slidesToShow: 3 } }, { breakpoint: 768, settings : { slidesToShow: 2 } }, { breakpoint: 320, settings : { slidesToShow: 1 , slidesToScroll: 1 } }  ]
  });
});

// Slick : END 

$(document).ready(function() {

	var dragging = false;
	$("body").on("mousedown", function(e) {
	  var x = e.screenX;
	  var y = e.screenY;
	  dragging = false;
	  $("body").on("mousemove", function(e) {
		if (Math.abs(x - e.screenX) > 5 || Math.abs(y - e.screenY) > 5) {
		  dragging = true;
		}
	  });
	});
	
	$("body").on("mouseup", function(e) {
	  $("body").off("mousemove");
	});

	function makeid(length) {
	   var result           = '';
	   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	   var charactersLength = characters.length;
	   for ( var i = 0; i < length; i++ ) {
		  result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   }
	   return result;
	}



	$(".plyr_load_in").on('click', function(e) { 
				
		e.preventDefault();
		e.stopImmediatePropagation();
	
		if(dragging){
			return false;
		}   

		var el = $(this);
		var id = makeid(5);
		var video = el.attr('data-video-id');
		var title = el.find('.more-desc').text();

		var dialog = bootbox.dialog({
			className: 'uor-modal-plyr',
			size: "large",
			message: '<span id="title_'+id+'" class="sr-only">Player for  ' + title + '</span><div class="plyr__video-embed js-player" id="'+id+'"><iframe src="https://www.youtube.com/embed/'+video+'"allowfullscreenallowtransparencyallow="autoplay"></iframe></div>',
			onHidden: function(e) {
				el.focus();
			},       
			onShow: function(e) {
				
			 $('.modal').attr('aria-labelledby', 'title_' + id );	
			 $('.bootbox-close-button').attr('aria-hidden', false ).attr('aria-label', 'Close video popup');	
//			 var player = new Plyr('#'+id, { fullscreen:{ enabled: true, fallback: true, iosNative: true, container: null }});
//			 $('.plyr__control').focus();			

			}
		});    	


	});

});


/*************************************************************************** bootstrap radio buttons ***********************************************************************************************/
      
// Bootstrap Radio buttons : BEGIN
  
//   function quickfitText() {
      
    
//   }

$("input[name=options]").change(function () {
    // alert(this.value);
    if(this.value == "U")
    {
        // alert("Yes it U"); // Postgraduate
        document.getElementById("subject-list-post").classList.add('menu-hide');
        document.getElementById("subject-list-under").classList.remove('menu-hide');
    }
    else
    {
        // alert("Yes it P"); // Undergrduate
        document.getElementById("subject-list-under").classList.add('menu-hide');
        document.getElementById("subject-list-post").classList.remove('menu-hide');
    }
});

// if(document.getElementById('option1').clicked == true) {
//    alert("option1 was clicked");
// }

// $( "#mydiv" ).hasClass( "foo" )
  
//   $(window).on("resize orientationchange",function() {
//       quickfitText(true); 
//   });

  
  // Bootstrap menu radio buttons : END


/*! modernizr 3.6.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-backgroundcliptext-setclasses !*/
!function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,s,i,l;for(var a in S)if(S.hasOwnProperty(a)){if(e=[],n=S[a],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,s=0;s<e.length;s++)i=e[s],l=i.split("."),1===l.length?Modernizr[l[0]]=o:(!Modernizr[l[0]]||Modernizr[l[0]]instanceof Boolean||(Modernizr[l[0]]=new Boolean(Modernizr[l[0]])),Modernizr[l[0]][l[1]]=o),C.push((o?"":"no-")+l.join("-"))}}function s(e){var n=_.className,t=Modernizr._config.classPrefix||"";if(x&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),x?_.className.baseVal=n:_.className=n)}function i(e,n){return!!~(""+e).indexOf(n)}function l(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):x?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function a(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function u(e,n){return function(){return e.apply(n,arguments)}}function f(e,n,t){var o;for(var s in e)if(e[s]in n)return t===!1?e[s]:(o=n[e[s]],r(o,"function")?u(o,t||n):o);return!1}function c(n,t,r){var o;if("getComputedStyle"in e){o=getComputedStyle.call(e,n,t);var s=e.console;if(null!==o)r&&(o=o.getPropertyValue(r));else if(s){var i=s.error?"error":"log";s[i].call(s,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!t&&n.currentStyle&&n.currentStyle[r];return o}function d(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function p(){var e=n.body;return e||(e=l(x?"svg":"body"),e.fake=!0),e}function m(e,t,r,o){var s,i,a,u,f="modernizr",c=l("div"),d=p();if(parseInt(r,10))for(;r--;)a=l("div"),a.id=o?o[r]:f+(r+1),c.appendChild(a);return s=l("style"),s.type="text/css",s.id="s"+f,(d.fake?d:c).appendChild(s),d.appendChild(c),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(n.createTextNode(e)),c.id=f,d.fake&&(d.style.background="",d.style.overflow="hidden",u=_.style.overflow,_.style.overflow="hidden",_.appendChild(d)),i=t(c,e),d.fake?(d.parentNode.removeChild(d),_.style.overflow=u,_.offsetHeight):c.parentNode.removeChild(c),!!i}function y(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(d(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var s=[];o--;)s.push("("+d(n[o])+":"+r+")");return s=s.join(" or "),m("@supports ("+s+") { #modernizr { position: absolute; } }",function(e){return"absolute"==c(e,null,"position")})}return t}function g(e,n,o,s){function u(){c&&(delete P.style,delete P.modElem)}if(s=r(s,"undefined")?!1:s,!r(o,"undefined")){var f=y(e,o);if(!r(f,"undefined"))return f}for(var c,d,p,m,g,v=["modernizr","tspan","samp"];!P.style&&v.length;)c=!0,P.modElem=l(v.shift()),P.style=P.modElem.style;for(p=e.length,d=0;p>d;d++)if(m=e[d],g=P.style[m],i(m,"-")&&(m=a(m)),P.style[m]!==t){if(s||r(o,"undefined"))return u(),"pfx"==n?m:!0;try{P.style[m]=o}catch(h){}if(P.style[m]!=g)return u(),"pfx"==n?m:!0}return u(),!1}function v(e,n,t,o,s){var i=e.charAt(0).toUpperCase()+e.slice(1),l=(e+" "+E.join(i+" ")+i).split(" ");return r(n,"string")||r(n,"undefined")?g(l,n,o,s):(l=(e+" "+N.join(i+" ")+i).split(" "),f(l,n,t))}function h(e,n,r){return v(e,t,t,n,r)}var C=[],S=[],w={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){S.push({name:e,fn:n,options:t})},addAsyncTest:function(e){S.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=w,Modernizr=new Modernizr;var _=n.documentElement,x="svg"===_.nodeName.toLowerCase(),b={elem:l("modernizr")};Modernizr._q.push(function(){delete b.elem});var P={style:b.elem.style};Modernizr._q.unshift(function(){delete P.style});var z="Moz O ms Webkit",E=w._config.usePrefixes?z.split(" "):[];w._cssomPrefixes=E;var N=w._config.usePrefixes?z.toLowerCase().split(" "):[];w._domPrefixes=N,w.testAllProps=v,w.testAllProps=h,Modernizr.addTest("backgroundcliptext",function(){return h("backgroundClip","text")}),o(),s(C),delete w.addTest,delete w.addAsyncTest;for(var k=0;k<Modernizr._q.length;k++)Modernizr._q[k]();e.Modernizr=Modernizr}(window,document);


if (Modernizr.backgroundcliptext) {    
    $('.clip').addClass('clip-add');
}