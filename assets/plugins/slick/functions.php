<?php 

function build_slide($slide){
    
    $id = $slide['incoming-url'];
    $url = $slide['url'];
    $title = $slide['title'];
    $url = $slide['url'];
    $image = $slide['image'];
    $color = $slide['color'];
    if(!empty($color)){ $color_text = $color . '-text';} else { $color_text ='';}
    if(!empty($color)){ $color_before = $color . '-before';} else { $color_before ='';}
    
    
    ?>
    <div class="uor-slide" style="overflow: hidden;" data-slick-id="<?php echo $id;?>" data-href="<?php echo $url;?>">
        <div class="uor-slide-inner">
            
            <div class="container h-100">
            
                <div class="row align-items-end h-100">
                
                    <div class="col white-text text-inner">
                        
                        
                        <?php if(empty($title)) { ?>
                        
                       <span class="master-advantage rdg-white-text"> <span style="font-size: 1.6rem;">Postgraduate</span> <br/><light>A</light> <span class="rdg-red-text">Master</span> <br/>Advantage</span><br/>
                        <a class="find-out-more <?php echo $color_before;?>">Register your interest.  <i class="fal fa-fw fa-chevron-right"></i></a>
                        
                        <?php } else { ?> 

                        <h3>Become a <br/> <span class="bold <?php echo $color_text;?>">Master</span> in</h3>
                        <h2 class="<?php echo $id;?>"><?php echo $title;?></h2>
                        <a class="find-out-more <?php echo $color_before;?>">Find out more  <i class="fal fa-fw fa-chevron-right"></i></a>
                        <?php } ?>
                        
                        
                    </div>
                    
                </div>
                
            </div>
            
            
            <div class="text-diagonal"></div>
                        
            <picture>
               <!-- Extra Large Desktops -->
                
            <?php if(!empty($image['superlarge']))  {?> 
               <source
                 media="(min-width: 75em)"
                 srcset="<?php echo F__IMAGES . $image['superlarge'];?>">
            <?php } ?> 
                
            <?php if(!empty($image['desktop']))  {?> 
               <source
                 media="(min-width: 62em)"
                 srcset="<?php echo F__IMAGES . $image['desktop'];?>">
            <?php } ?> 
                
            <?php if(!empty($image['tablet']))  {?> 
               <source
                 media="(min-width: 48em)"
                 srcset="<?php echo F__IMAGES . $image['tablet'];?>">
            <?php } ?> 
                
            <?php if(!empty($image['mobile-landscape']))  {?>            
               <source
                 media="(min-width: 34em)"
                 srcset="<?php echo F__IMAGES . $image['mobile-landscape'];?>">
            <?php } ?> 
                
               <img
                 src="<?php echo F__IMAGES . $image['desktop'];?>"
                 srcset="<?php echo F__IMAGES . $image['mobile'];?>"
                 alt="<?php echo $image['alt'];?>"
                 title="<?php echo $image['title'];?>"    
                >
                
             </picture>            
            
        </div>
        
    </div>
<?php
}
?>