// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const cleanCSS = require('gulp-clean-css');
var replace = require('gulp-replace');
var browserSync = require('browser-sync').create();


// File paths
const files = {
    scssPath: [ 
        'css/scss/*.scss'
    ],

    cssPath: [ 
        // 'css/core-build.css', 
        'css/build.css', 
        'assets/plugins/animate/animate.min.css', 
        'assets/plugins/cookie-consent/css/cookieconsent.min.css',  
        'node_modules/@accessible360/accessible-slick/slick/slick.min.css', 
        'node_modules/@accessible360/accessible-slick/slick/accessible-slick-theme.min.css', 
        'css/core.css',
    ],

	jqueryPath: [
        'vendor/components/jquery/jquery.min.js',
    ],
    

    jsPath: [ 
        'vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js', 
        'node_modules/focus-visible/dist/focus-visible.min.js', 
        'assets/plugins/cookie-consent/js/cookieconsent.min.js', 
        'node_modules/vanilla-lazyload/dist/lazyload.min.js', 
        'node_modules/@accessible360/accessible-slick/slick/slick.min.js', 
        'node_modules/bootbox/dist/bootbox.all.min.js', 
        'js/core.js'
    ]
	
	
	//  'node_modules/plyr/dist/plyr.polyfilled.min.js',
}

// Sass task: compiles the style.scss file into style.css
// function scssTask(){    
//     return src(files.scssPath)
//         .pipe(sourcemaps.init()) // initialize sourcemaps first
//         .pipe(sass()) // compile SCSS to CSS
//         // .pipe(postcss([ autoprefixer(), cssnano() ])) // PostCSS plugins
//         // .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
//         .pipe(dest('dist')
//     ); // put final CSS in dist folder
// }

function scssTask(){    
    return src(files.scssPath)
        .pipe(sass()) // compile SCSS to CSS
        .pipe(dest('css')
    ); // put final CSS in dist folder
}

// Sass task: compiles the style.scss file into style.css
function cssTask(){    
    return src(files.cssPath)
        .pipe(concat('core.min.css'))
		.pipe(sourcemaps.init())
		.pipe(cleanCSS())
		.pipe(sourcemaps.write())	
        .pipe(dest('dist')
			  
    ); // put final CSS in dist folder
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src(
        files.jsPath
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
        )
        .pipe(concat('core.min.js'))
        .pipe(uglify())
        .pipe(dest('dist')
    );
}

// JS task: concatenates and uglifies JS files to script.js
function jQueryTask(){
    return src(
        files.jqueryPath
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
        )
        .pipe(concat('jquery.min.js'))
        .pipe(uglify())
        .pipe(dest('dist')
    );
}

// Cachebust
function cacheBustTask(){
    var cbString = new Date().getTime();
    return src(['index.html'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(dest('.'));
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){
    watch(files.jsPath,
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( jsTask, jQueryTask, cssTask),
            cacheBustTask
        )
    );   
    watch(files.cssPath,
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( cssTask, jsTask),
            cacheBustTask
        )
    );
    watch(files.scssPath,
        {interval: 1000, usePolling: true}, //Makes docker work
        series(
            parallel( scssTask ), 
            cssTask
        )
    );     	
	
	
	
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    parallel(jsTask, jQueryTask, cssTask),  //scssTask
    cacheBustTask,
    watchTask
);